CREATE TABLE `voto` (
  `id_usuario` int,
  `id_opcao_voto` int,
  FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  FOREIGN KEY (`id_opcao_voto`) REFERENCES `opcao_voto` (`id_opcao_voto`),
  CONSTRAINT `unique_voto` UNIQUE (`id_usuario`,`id_opcao_voto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;