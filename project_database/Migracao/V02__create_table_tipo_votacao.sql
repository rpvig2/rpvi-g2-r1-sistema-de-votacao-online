CREATE TABLE `tipo_votacao` (
  `id_tipo_votacao` int NOT NULL,
  `descricao` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_votacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `tipo_votacao` (`id_tipo_votacao`, `descricao`) VALUES
(0, 'PADRAO'),
(1, 'CUSTOMIZADA');

