ALTER TABLE `opcao_voto_customizada` RENAME TO `opcao_voto`;       

ALTER TABLE `opcao_voto` CHANGE `id_opcao_voto_customizada` `id_opcao_voto` INT; 

CREATE TABLE `descricao_opcao_voto` (
  `id_descricao` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(200) NOT NULL,  
  PRIMARY KEY (`id_descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `descricao_opcao_voto` (`id_descricao`,`descricao`) VALUES
(1, 'Abstenção'),
(2, 'Favorável'),
(3, 'Contrário');

ALTER TABLE `opcao_voto` CHANGE `descricao` `id_descricao` INT;

ALTER TABLE `opcao_voto`
    ADD FOREIGN KEY (`id_descricao`) REFERENCES `descricao_opcao_voto`(`id_descricao`);

ALTER TABLE `opcao_voto` 
  ADD UNIQUE `unique_opcao`(`id_descricao`, `id_item_pauta`);