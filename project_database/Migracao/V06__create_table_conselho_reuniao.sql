CREATE TABLE `conselho_reuniao` (
  `id_usuario` int(11) NOT NULL,
  `id_reuniao` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`,`id_reuniao`),
  FOREIGN KEY (`id_reuniao`) REFERENCES `reuniao` (`id_reuniao`),
  FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  UNIQUE (`id_usuario`,`id_reuniao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `conselho_reuniao` (`id_usuario`, `id_reuniao`) VALUES
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(2, 2), 
(3, 2);