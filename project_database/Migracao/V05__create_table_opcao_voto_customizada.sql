CREATE TABLE `opcao_voto_customizada` (
  `id_opcao_voto_customizada` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL,
  `id_item_pauta` int(11) NOT NULL,
  PRIMARY KEY (`id_opcao_voto_customizada`),
  FOREIGN KEY (`id_item_pauta`) REFERENCES `item_pauta` (`id_item_pauta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;