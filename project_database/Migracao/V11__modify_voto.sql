SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `voto`
    ADD COLUMN `id_item_pauta` int NOT NULL;

ALTER TABLE `voto`   
    ADD FOREIGN KEY (`id_item_pauta`) REFERENCES `item_pauta`(`id_item_pauta`);

ALTER TABLE `voto`   
    ADD CONSTRAINT `unique_voto_item_pauta` UNIQUE (`id_usuario`,`id_item_pauta`);

SET FOREIGN_KEY_CHECKS=1;