ALTER TABLE `status_reuniao` RENAME TO `status`;

ALTER TABLE `item_pauta` 
    DROP COLUMN `aberto_votacao`;

ALTER TABLE `item_pauta` 
    ADD COLUMN `status` INT NOT NULL DEFAULT '1';

ALTER TABLE `item_pauta`
    ADD FOREIGN KEY (`status`) REFERENCES `status`(`id_status`);