CREATE TABLE `item_pauta` (
  `id_item_pauta` int NOT NULL AUTO_INCREMENT,
  `descricao` varchar(200) NOT NULL,
  `id_reuniao` int(11) NOT NULL,
  `tipo_votacao` int(11) NOT NULL,
  `aberto_votacao` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_item_pauta`),
  FOREIGN KEY (`id_reuniao`) REFERENCES `reuniao` (`id_reuniao`),
  FOREIGN KEY (`tipo_votacao`) REFERENCES `tipo_votacao` (`id_tipo_votacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `item_pauta` (`id_item_pauta`, `descricao`, `id_reuniao`, `tipo_votacao`, `aberto_votacao`) VALUES
(1, 'Item de Pauta 1', 1, 0, 0),
(2, 'Item de Pauta 2', 1, 0, 0),
(3, 'Item de Pauta 3', 1, 0, 0),
(4, 'Item de Pauta 4', 1, 0, 0),
(5, 'Item de Pauta 5', 1, 0, 0),
(6, 'Item de Pauta 6', 1, 0, 0);

