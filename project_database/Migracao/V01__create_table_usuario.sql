CREATE TABLE `usuario` (
  `id_usuario` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `nome_usuario` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE (`nome_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `usuario` (`id_usuario`, `nome`, `nome_usuario`, `senha`) VALUES
(1, 'Lucas', 'Moderador', '123'),
(2, 'Yury', 'Conselheiro', '123'),
(3, 'Juliana', 'Conselheiro1', '123'),
(4, 'Lucas C', 'Conselheiro2', '123');



