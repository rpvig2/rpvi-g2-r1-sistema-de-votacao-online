CREATE TABLE `reuniao` (
  `id_reuniao` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `data` date NOT NULL,
  `nome_colegiado` varchar(100) DEFAULT NULL,
  `id_moderador` int NOT NULL,
  `aberta` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_reuniao`),
  FOREIGN KEY (`id_moderador`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `reuniao` (`id_reuniao`, `nome`, `data`, `nome_colegiado`, `id_moderador`, `aberta`) VALUES
(1, 'Reuniao 1', '2018-05-05', 'Colegiado 1', 1, 0),
(2, 'Reuniao 2', '2018-09-03', 'Colegiado 2', 1, 0),
(3, 'Reuniao 3', '2018-05-05', 'Colegiado 3', 1, 0),
(4, 'Reuniao 4', '2018-05-05', 'Colegiado 1', 1, 0),
(5, 'Reuniao 5', '2018-09-03', 'Colegiado 2', 1, 0),
(6, 'Reuniao 6', '2018-05-05', 'Colegiado 3', 1, 0);

