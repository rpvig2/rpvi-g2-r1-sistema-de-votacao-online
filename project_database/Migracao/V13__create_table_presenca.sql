CREATE TABLE `presenca` (
  `id_usuario` int,
  `id_item_pauta` int,
  `status_voto` tinyint(1) NOT NULL,
  FOREIGN KEY (`id_item_pauta`) REFERENCES `item_pauta`(`id_item_pauta`),
  FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),  
  CONSTRAINT `unique_presenca` UNIQUE (`id_usuario`,`id_item_pauta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;