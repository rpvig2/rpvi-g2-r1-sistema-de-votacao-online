CREATE TABLE `status_reuniao` (
  `id_status` int NOT NULL,
  `descricao` varchar(45) NOT NULL,  
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `status_reuniao` (`id_status`, `descricao`) VALUES
(1, 'FECHADA'),
(2, 'ABERTA'),
(3, 'FINALIZADA');

ALTER TABLE `reuniao`
    ADD COLUMN `status_reuniao` int NOT NULL DEFAULT '1';       

ALTER TABLE `reuniao`    
    ADD FOREIGN KEY (`status_reuniao`) REFERENCES `status_reuniao`(`id_status`);

ALTER TABLE `reuniao`    
    DROP COLUMN aberta;

