# RPVI - G2 R1 - Sistema de Vota��o Online

## Instala��o

1 - Fazer o clone do sistema

```
git clone https://GuilhermeBolfe@bitbucket.org/rpvig2/rpvi-g2-r1-sistema-de-votacao-online.git
```

2 - Importar o script do banco de dados no que est� arquivo ```project_database/webvotocodeigniter.sql```

3 - Editar o arquivo ```application/config/databases.php``` no sitema configurando nome do banco de dados, nome de usu�rio, senha e endere�o do banco

4 - Editar o arquivo ```application/config/config.php``` configurando a url base do projeto

## Requisitos

1 - O sistema foi desenvolvido usando o framework Codeigniter na vers�o 3.1.9 e deve funcionar sem problemas em vers�es do PHP 5.7 para cima, bem como MySql vers�es 5.0 pra cima.
