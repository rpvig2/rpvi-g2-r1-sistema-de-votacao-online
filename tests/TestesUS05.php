{
  "id": "69048031-8f1a-400b-90fb-47e369b4b5d0",
  "version": "1.1",
  "name": "GP2 - GP1",
  "url": "http://localhost/",
  "tests": [{
    "id": "cfdb6584-bdc4-4d86-8585-30683ab7fa2a",
    "name": "Teste Votação Padrão",
    "commands": [{
      "id": "35e6ff4d-906d-4de2-a458-f7f356fc3394",
      "comment": "",
      "command": "open",
      "target": "/rpvi/login",
      "targets": [],
      "value": ""
    }, {
      "id": "d6549f76-fe7f-4bc8-bb49-2f17784e7e21",
      "comment": "",
      "command": "setWindowSize",
      "target": "1382x744",
      "targets": [],
      "value": ""
    }, {
      "id": "2bd2917a-9db7-4aa9-b752-0b7305542911",
      "comment": "",
      "command": "click",
      "target": "id=inputUserName",
      "targets": [
        ["id=inputUserName", "id"],
        ["name=inputUserName", "name"],
        ["css=#inputUserName", "css"],
        ["css=#inputUserName", "css:finder"],
        ["xpath=//input[@id='inputUserName']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/input", "xpath:idRelative"],
        ["xpath=//input", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "cc06a5dc-d265-4b7f-bdb7-dd0d6a2cb229",
      "comment": "",
      "command": "type",
      "target": "id=inputUserName",
      "targets": [
        ["id=inputUserName", "id"],
        ["name=inputUserName", "name"],
        ["css=#inputUserName", "css"],
        ["css=#inputUserName", "css:finder"],
        ["xpath=//input[@id='inputUserName']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/input", "xpath:idRelative"],
        ["xpath=//input", "xpath:position"]
      ],
      "value": "moderador"
    }, {
      "id": "a60cc176-27fe-4555-83f8-1804e5a45bfb",
      "comment": "",
      "command": "type",
      "target": "id=inputPassword",
      "targets": [
        ["id=inputPassword", "id"],
        ["name=inputPassword", "name"],
        ["css=#inputPassword", "css"],
        ["css=#inputPassword", "css:finder"],
        ["xpath=//input[@id='inputPassword']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/input[2]", "xpath:idRelative"],
        ["xpath=//input[2]", "xpath:position"]
      ],
      "value": "123"
    }, {
      "id": "d25300a5-3f7e-4762-acb0-7359d483c2ab",
      "comment": "",
      "command": "click",
      "target": "css=.btn",
      "targets": [
        ["css=.btn", "css:finder"],
        ["xpath=//button[@type='submit']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "75b9e640-457f-4f66-91d2-76236178111d",
      "comment": "",
      "command": "click",
      "target": "css=button.btn.btn-lg",
      "targets": [
        ["css=button.btn.btn-lg", "css"],
        ["css=tr:nth-child(1) .btn", "css:finder"],
        ["xpath=//button[@type='submit']", "xpath:attributes"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "24dd3b7e-f72e-4bbe-8187-6c14e1841ff5",
      "comment": "",
      "command": "click",
      "target": "css=button.btn.btn-lg",
      "targets": [
        ["css=button.btn.btn-lg", "css"],
        ["css=tr:nth-child(1) .btn", "css:finder"],
        ["xpath=//button[@type='submit']", "xpath:attributes"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "fefcf86e-043a-4d1c-a945-3ccf51c11cc5",
      "comment": "",
      "command": "assertText",
      "target": "css=#defaultTable > tbody > tr > td",
      "targets": [
        ["css=#defaultTable > tbody > tr > td", "css"],
        ["css=#defaultTable tr:nth-child(1) > td", "css:finder"],
        ["xpath=//table[@id='defaultTable']/tbody/tr/td", "xpath:idRelative"],
        ["xpath=//table[2]/tbody/tr/td", "xpath:position"]
      ],
      "value": "Abstenção"
    }, {
      "id": "ae7c3db2-5e81-4eab-b352-5faf74c037fd",
      "comment": "",
      "command": "assertText",
      "target": "css=tr:nth-child(2) > td",
      "targets": [
        ["css=tr:nth-child(2) > td", "css:finder"],
        ["xpath=//table[@id='defaultTable']/tbody/tr[2]/td", "xpath:idRelative"],
        ["xpath=//tr[2]/td", "xpath:position"]
      ],
      "value": "Favorável"
    }, {
      "id": "5130e4af-fe2c-4f31-baed-320801d89d0c",
      "comment": "",
      "command": "assertText",
      "target": "css=tr:nth-child(3) > td",
      "targets": [
        ["css=tr:nth-child(3) > td", "css:finder"],
        ["xpath=//table[@id='defaultTable']/tbody/tr[3]/td", "xpath:idRelative"],
        ["xpath=//tr[3]/td", "xpath:position"]
      ],
      "value": "Contrário"
    }]
  }, {
    "id": "68a01097-0c8a-4de3-abab-35baa50bd32e",
    "name": "Teste Votação Customizada",
    "commands": [{
      "id": "423ab790-a619-4521-a0b5-8a4886038f9c",
      "comment": "",
      "command": "open",
      "target": "/rpvi/login",
      "targets": [],
      "value": ""
    }, {
      "id": "0625d745-a237-4c1a-9fbe-7f6b96016b8c",
      "comment": "",
      "command": "setWindowSize",
      "target": "1382x744",
      "targets": [],
      "value": ""
    }, {
      "id": "8e31a70a-b6bb-4703-b432-4b3f35c90868",
      "comment": "",
      "command": "click",
      "target": "id=inputUserName",
      "targets": [
        ["id=inputUserName", "id"],
        ["name=inputUserName", "name"],
        ["css=#inputUserName", "css"],
        ["css=#inputUserName", "css:finder"],
        ["xpath=//input[@id='inputUserName']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/input", "xpath:idRelative"],
        ["xpath=//input", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "c92532cd-257b-4c21-8d29-e98968b33a83",
      "comment": "",
      "command": "type",
      "target": "id=inputUserName",
      "targets": [
        ["id=inputUserName", "id"],
        ["name=inputUserName", "name"],
        ["css=#inputUserName", "css"],
        ["css=#inputUserName", "css:finder"],
        ["xpath=//input[@id='inputUserName']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/input", "xpath:idRelative"],
        ["xpath=//input", "xpath:position"]
      ],
      "value": "moderador"
    }, {
      "id": "142f1e0c-4abb-4a63-a887-30b8417e72d1",
      "comment": "",
      "command": "type",
      "target": "id=inputPassword",
      "targets": [
        ["id=inputPassword", "id"],
        ["name=inputPassword", "name"],
        ["css=#inputPassword", "css"],
        ["css=#inputPassword", "css:finder"],
        ["xpath=//input[@id='inputPassword']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/input[2]", "xpath:idRelative"],
        ["xpath=//input[2]", "xpath:position"]
      ],
      "value": "123"
    }, {
      "id": "5baade03-9a7c-43d4-9dc5-b1986f79be03",
      "comment": "",
      "command": "click",
      "target": "css=.btn",
      "targets": [
        ["css=.btn", "css:finder"],
        ["xpath=//button[@type='submit']", "xpath:attributes"],
        ["xpath=//form[@id='loginForm']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "8e68ac50-5297-4aae-bb92-e902735c79b5",
      "comment": "",
      "command": "click",
      "target": "css=button.btn.btn-lg",
      "targets": [
        ["css=button.btn.btn-lg", "css"],
        ["css=tr:nth-child(1) .btn", "css:finder"],
        ["xpath=//button[@type='submit']", "xpath:attributes"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "ea7dbbe0-af40-46dc-9c4b-89e2b7ac664d",
      "comment": "",
      "command": "click",
      "target": "css=button.btn.btn-lg",
      "targets": [
        ["css=button.btn.btn-lg", "css"],
        ["css=tr:nth-child(1) .btn", "css:finder"],
        ["xpath=//button[@type='submit']", "xpath:attributes"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "b531533f-0653-4eb5-aaba-ca506ed934dd",
      "comment": "",
      "command": "click",
      "target": "id=customField",
      "targets": [
        ["id=customField", "id"],
        ["name=customField", "name"],
        ["css=#customField", "css"],
        ["css=#customField", "css:finder"],
        ["xpath=//input[@id='customField']", "xpath:attributes"],
        ["xpath=//div[@id='body']/input", "xpath:idRelative"],
        ["xpath=//div/input", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "86932182-a531-4ae1-bb22-8edd0d8c80e4",
      "comment": "",
      "command": "type",
      "target": "id=customField",
      "targets": [
        ["id=customField", "id"],
        ["name=customField", "name"],
        ["css=#customField", "css"],
        ["css=#customField", "css:finder"],
        ["xpath=//input[@id='customField']", "xpath:attributes"],
        ["xpath=//div[@id='body']/input", "xpath:idRelative"],
        ["xpath=//div/input", "xpath:position"]
      ],
      "value": "Opção voto 1"
    }, {
      "id": "297a8693-d672-47ce-b8e0-54e4b26f80b9",
      "comment": "",
      "command": "click",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "46351735-79c1-4cbc-a811-92e5bc57903e",
      "comment": "",
      "command": "mouseOver",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "d69e73e5-5a01-4c02-8aae-6635e73fe581",
      "comment": "",
      "command": "mouseOut",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "bab54ea1-bd7d-426e-bc3f-9075e34be8a5",
      "comment": "",
      "command": "click",
      "target": "id=customField",
      "targets": [
        ["id=customField", "id"],
        ["name=customField", "name"],
        ["css=#customField", "css"],
        ["css=#customField", "css:finder"],
        ["xpath=//input[@id='customField']", "xpath:attributes"],
        ["xpath=//div[@id='body']/input", "xpath:idRelative"],
        ["xpath=//div/input", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "54616e25-9288-417a-b761-50752ce48a10",
      "comment": "",
      "command": "type",
      "target": "id=customField",
      "targets": [
        ["id=customField", "id"],
        ["name=customField", "name"],
        ["css=#customField", "css"],
        ["css=#customField", "css:finder"],
        ["xpath=//input[@id='customField']", "xpath:attributes"],
        ["xpath=//div[@id='body']/input", "xpath:idRelative"],
        ["xpath=//div/input", "xpath:position"]
      ],
      "value": "Opção Voto 2"
    }, {
      "id": "3dc7336f-1a2e-4e0e-85ca-68c4391509a7",
      "comment": "",
      "command": "click",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "fb53e381-8b3b-47ff-a0b4-fb2e2d09607c",
      "comment": "",
      "command": "mouseOver",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "2229f360-90df-4488-9ab3-2fd3b3556bcc",
      "comment": "",
      "command": "mouseOut",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "ab9b518e-7c5d-4f3a-97c3-7ec3fc38b69d",
      "comment": "",
      "command": "click",
      "target": "id=customField",
      "targets": [
        ["id=customField", "id"],
        ["name=customField", "name"],
        ["css=#customField", "css"],
        ["css=#customField", "css:finder"],
        ["xpath=//input[@id='customField']", "xpath:attributes"],
        ["xpath=//div[@id='body']/input", "xpath:idRelative"],
        ["xpath=//div/input", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "dc83e11a-c468-4a99-a7e2-5d171ca87abb",
      "comment": "",
      "command": "type",
      "target": "id=customField",
      "targets": [
        ["id=customField", "id"],
        ["name=customField", "name"],
        ["css=#customField", "css"],
        ["css=#customField", "css:finder"],
        ["xpath=//input[@id='customField']", "xpath:attributes"],
        ["xpath=//div[@id='body']/input", "xpath:idRelative"],
        ["xpath=//div/input", "xpath:position"]
      ],
      "value": "Opção Voto 3"
    }, {
      "id": "a1fb124d-e2bb-4024-979a-1cc37d321f9f",
      "comment": "",
      "command": "click",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "13bac8ed-c1f1-4914-9bc2-b27dfcbb21bd",
      "comment": "",
      "command": "mouseOver",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "45ba1a5a-58b5-4463-ba40-b32dfc441c31",
      "comment": "",
      "command": "mouseOut",
      "target": "name=customButton",
      "targets": [
        ["name=customButton", "name"],
        ["css=button[name=\"customButton\"]", "css"],
        ["css=.btn:nth-child(5)", "css:finder"],
        ["xpath=//button[@name='customButton']", "xpath:attributes"],
        ["xpath=//div[@id='body']/button", "xpath:idRelative"],
        ["xpath=//button", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "c3ff89ac-e22d-4028-b1f7-386df7ee0609",
      "comment": "",
      "command": "click",
      "target": "css=span.switch-label",
      "targets": [
        ["css=span.switch-label", "css"],
        ["css=.switch-label", "css:finder"],
        ["xpath=//div[@id='body']/label/span", "xpath:idRelative"],
        ["xpath=//span", "xpath:position"]
      ],
      "value": ""
    }, {
      "id": "eaa8798d-27de-457d-91a6-4bd2179103fc",
      "comment": "",
      "command": "assertValue",
      "target": "css=td[name=\"arrayVotingOptions\"]",
      "targets": [
        ["css=td[name=\"arrayVotingOptions\"]", "css"],
        ["css=tr:nth-child(2) > td:nth-child(2)", "css:finder"],
        ["xpath=//td[@name='arrayVotingOptions']", "xpath:attributes"],
        ["xpath=//tbody[@id='tableBody']/tr[2]/td[2]", "xpath:idRelative"],
        ["xpath=//tr[2]/td[2]", "xpath:position"]
      ],
      "value": "Opção voto 2"
    }]
  }, {
    "id": "46fccfff-fd3f-460f-a792-438ac770d7c7",
    "name": "Teste Votação Custon",
    "commands": []
  }],
  "suites": [{
    "id": "ba720956-a6db-40fb-b571-fb77b5bdb2a0",
    "name": "Default Suite",
    "persistSession": false,
    "parallel": false,
    "timeout": 300,
    "tests": ["cfdb6584-bdc4-4d86-8585-30683ab7fa2a"]
  }],
  "urls": ["http://localhost/"],
  "plugins": []
}