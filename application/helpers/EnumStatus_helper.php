<?php
function getStatusId($status)
{
    switch ($status) {
        case "FECHADA":
            $id_status = 1;
            break;
        case "ABERTA":
            $id_status = 2;
            break;
        case"FINALIZADA":
            $id_status = 3;
            break;
        default:
            $id_status = 1;
            break;
    }

    return $id_status;

}
