<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VotoModel extends CI_Model
{

    private $idUsuario;
    
    private $idOpcaoDeVoto;

    private $idItemPauta;

    private $usuario;

    private $opcaoDeVoto;

    public function __construct()
    {
        parent::__construct();        
    }

    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
    
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }
    
    public function getIdOpcaoDeVoto()
    {
        return $this->idOpcaoDeVoto;
    }
    
    public function setIdOpcaoDeVoto($idOpcaoDeVoto)
    {
        $this->idOpcaoDeVoto = $idOpcaoDeVoto;
    }
    
    public function getIdItemPauta()
    {
        return $this->idItemPauta;
    }
    
    public function setIdItemPauta($idItemPauta)
    {
        $this->idItemPauta = $idItemPauta;
    }
     
    public function getUsuario()
    {
        return $this->usuario;
    }
    
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    public function getOpcaoDeVoto()
    {
        return $this->opcaoDeVoto;
    }
  
    public function setOpcaoDeVoto($opcaoDeVoto)
    {
        $this->opcaoDeVoto = $opcaoDeVoto;
    }
}