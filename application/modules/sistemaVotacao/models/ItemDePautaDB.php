<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ItemDePautaDB extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ItemDePautaModel');
        $this->load->model('OpcaoDeVotoModel');
        $this->load->model('OpcaoDeVotoDB');
        $this->load->database();
        $this->load->helper('EnumStatus');
    }

    public function selectById($Id)
    {
        $this->db->from('item_pauta');
        $this->db->where('id_item_pauta', $Id);
        $query = $this->db->get();

        return $this->getData($query)[0];
    }


    public function selectByReuniaoId($reuniaoId)
    {
        $this->db->from('item_pauta');
        $this->db->where('id_reuniao', $reuniaoId);
        $query = $this->db->get();

        return $this->getData($query);
    }

    private function getData($query)
    {
        $listItemDePauta= new ArrayObject();

        foreach ($query->result() as $row) {
            $itemDePauta = new ItemDePautaModel();
            $itemDePauta->setId($row->id_item_pauta);
            $itemDePauta->setDescricao($row->descricao);
            $itemDePauta->setReuniaoId($row->id_reuniao);
            $itemDePauta->setType($this->getType($row->tipo_votacao));
            $itemDePauta->setStatus($this->getStatus($row->status));
            
            $opcaoDeVotoDB = new OpcaoDeVotoDB();
            $itemDePauta->setOpcaoDeVotoList($opcaoDeVotoDB->selectByItemPautaId($itemDePauta->getId()));
            
            $listItemDePauta->append($itemDePauta);
        }

        return $listItemDePauta;
    }

    private function getStatus($id_status)
    {
        $this->db->from('status');
        $this->db->where('id_status', $id_status);
        $query = $this->db->get();

        $row = $query->row();

        if (isset($row)) {
            return $row->descricao;
        }
    }


    private function getType($idType)
    {
        $this->db->from('tipo_votacao');
        $this->db->where('id_tipo_votacao', $idType);
        $query = $this->db->get();

        $row = $query->row();

        return $row->descricao;
    }

    public function updateVotacaoType($id, $idType)
    {
        $this->db->where('id_item_pauta', $id);
        $this->db->set('tipo_votacao', $idType);
        $this->db->update('item_pauta');
    }
    
    public function updateStatus($id, $status)
    {
        $id_status = getStatusId($status);

        $this->db->where('id_item_pauta', $id);
        $this->db->set('status', $id_status);
        $this->db->update('item_pauta');
    }

    public function getItemPautaAbertoByReuniaoId($reuniaoId){

        $id_status = getStatusId("ABERTA");

        $this->db->where('status', $id_status);
        $this->db->where('id_reuniao', $reuniaoId);
        $query = $this->db->get('item_pauta');
       
        $row = $query->row();

        if($this->getData($query)->count() == 0){
            return null;
        }else{
            return $this->getData($query)[0];
        }

    }

}
