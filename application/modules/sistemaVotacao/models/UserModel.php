<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends CI_Model implements JsonSerializable
{
    private $id;
    private $name;
    private $userName;
    private $password;
    private $reuniaoConselheiroList;
    private $reuniaoModeradorList;

    public function __construct()
    {
        parent::__construct();
        $this->reuniaoModeradorList = new ArrayObject();
        $this->reuniaoConselheiroList = new ArrayObject();
    }

    public function jsonSerialize() {
        return ['nome' => $this->name];                
    }

    public function getId()
    {
        return $this->id;
    }
   
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
    
    public function getReuniaoConselheiroList()
    {
        return $this->reuniaoConselheiroList;
    }
    
    public function setReuniaoConselheiroList($reuniaoConselheiroList)
    {
        $this->reuniaoConselheiroList = $reuniaoConselheiroList;
    }

    public function getReuniaoModeradorList()
    {
        return $this->reuniaoModeradorList;
    }

    public function setReuniaoModeradorList($reuniaoModeradorList)
    {
        $this->reuniaoModeradorList = $reuniaoModeradorList;
    }

    
}
