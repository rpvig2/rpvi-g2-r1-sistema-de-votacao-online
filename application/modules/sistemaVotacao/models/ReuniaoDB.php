<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ReuniaoDB extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ReuniaoModel');
        $this->load->model('UserModel');
        $this->load->database();
    }

    public function selectAsModerador($user)
    {
        $this->db->from('reuniao');
        $this->db->where('id_moderador', $user->getId());
        $query = $this->db->get();

        return $this->getData($query);
    }

    public function selectAsConselheiro($user)
    {
        $this->db->from('conselho_reuniao');
        $this->db->where('id_usuario', $user->getId());
        $query = $this->db->get();

        $listReunioes = new ArrayObject();

        foreach($query->result() as $row){
            $reuniao = $this->getById($row->id_reuniao);
            if (isset($reuniao)) {
                $listReunioes->append($reuniao);
            }
        }

        return $listReunioes;
    }

    public function selectAll(){
        
        $reunioes = array();
        
        $this->db->from('reuniao');
        $query = $this->db->get();

        foreach ($query->result() as $row) {
            if (isset($row)) {
                array_push($reunioes, $this->mountData($row));
            }
        }
        
        return $reunioes;
    }

    public function getById($reuniaoId)
    {
        $this->db->from('reuniao');
        $this->db->where('id_reuniao', $reuniaoId);
        $query = $this->db->get();

        $row = $query->row();

        if (isset($row)) {
            return $this->mountData($row);
        }

        return null;
    }

    private function getData($query)
    {
        $listReunioes = new ArrayObject();

        foreach ($query->result() as $row) {
            $reuniao = $this->mountData($row);
            if (isset($reuniao)) {
                $listReunioes->append($reuniao);
            }
        }

        return $listReunioes;
    }

    private function mountData($row)
    {
        $reuniao = new ReuniaoModel();
        $reuniao->setId($row->id_reuniao);
        $reuniao->setName($row->nome);
        $reuniao->setData($row->data);
        $reuniao->setColegiado($row->nome_colegiado);
        $reuniao->setStatus($this->getStatus($row->status_reuniao));

        return $reuniao;
    }

    private function getStatus($id_status){

        $this->db->from('status');
        $this->db->where('id_status', $id_status);
        $query = $this->db->get();

        $row = $query->row();

        if (isset($row)){
            return $row->descricao;
        }

    }


    public function updateAberta($id, $status){

        switch ($status){
            case "FECHADA":
                $id_status = 1;
                break;
            case "ABERTA":
                $id_status = 2;
                break;
            case"FINALIZADA":
                $id_status = 3;
                break;
            default:
                $id_status = 1;
                break;
        }

        $this->db->where('id_reuniao', $id);
        $this->db->set('status_reuniao', $id_status);
        $this->db->update('reuniao');

    }

}
