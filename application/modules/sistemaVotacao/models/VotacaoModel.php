<?php

defined('BASEPATH') or exit('No direct script access allowed');
class VotacaoModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('VotoModel');
        $this->load->model('VotoDB');
        $this->load->model('ItemDePautaModel');
        $this->load->model('ItemDePautaDB');
        $this->load->model('UserModel');
        $this->load->model('UserDB');
        $this->load->model('OpcaoDeVotoModel');
        $this->load->model('OpcaoDeVotoDB');
    }

    public function calcularResultado($itemDePauta, $data)
    {
        $votoDB = new VotoDB();
        $opcaoDeVotoDB = new OpcaoDeVotoDB();
        $userDB = new UserDB();

        $listVotos = $votoDB->selectByIdItemPauta($itemDePauta->getId());

        foreach ($listVotos as $voto) {
            $voto->setOpcaoDeVoto($opcaoDeVotoDB->selectById($voto->getIdOpcaoDeVoto()));
            $voto->setUsuario($userDB->selectById($voto->getIdUsuario()));

            foreach ($itemDePauta->getOpcaoDeVotoList() as $opcaoDeVoto) {
                if ($opcaoDeVoto->getId() === $voto->getIdOpcaoDeVoto()) {
                    $opcaoDeVoto->addVoto();
                    break;
                }
            }
        }

        

        $max['id'] = 0;
        $max['value'] = 0;

        //Set Porcentagem e procurando o com maior numero de votos
        foreach ($itemDePauta->getOpcaoDeVotoList() as $opcaoDeVoto) {
            if ($max['value'] < $opcaoDeVoto->getQuantidadeVotos()) {
                $max['value'] = $opcaoDeVoto->getQuantidadeVotos();
                $max['id'] = $opcaoDeVoto->getId();
            }

            $opcaoDeVoto->setPorcentagem($listVotos->count());
        }

        $semEmpate = true;

        //verificando se teve empate
        foreach ($itemDePauta->getOpcaoDeVotoList() as $opcaoDeVoto) {
            if ($opcaoDeVoto->getId() != $max['id']) {
                if ($max['value'] == $opcaoDeVoto->getQuantidadeVotos()) {
                    $semEmpate = false;
                    break;                    
                }
            }
        }

        foreach ($itemDePauta->getOpcaoDeVotoList() as $opcaoDeVoto) {
            if ($opcaoDeVoto->getId() == $max['id']) {
                $opcaoDeVoto->setVencedor(true);
                break;
            }
        }

        $data['listVotos'] = $listVotos;
        $data['itemDePauta'] = $itemDePauta;

        return $semEmpate;
    }
}
