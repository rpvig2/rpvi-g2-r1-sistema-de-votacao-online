<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PresencaDB extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PresencaModel');
        $this->load->database();
    }

    public function save($presenca)
    {
        try {
            $this->db->set('id_usuario', $presenca->getIdUsuario());
            $this->db->set('id_item_pauta', $presenca->getIdItemPauta());
            $this->db->set('status_voto', false);
            $this->db->insert('presenca');
            return null;
        } catch (Exception $e) {
            return $this->selectByIdUsuario($presenca->getIdUsuario());
        }
    }

    public function delete($presenca)
    {
        $this->db->where('id_item_pauta', $presenca->getIdItemPauta());
        $this->db->where('id_usuario', $presenca->getIdUsuario());
        $this->db->delete('presenca');
    }

    public function selectByIdItemPauta($idItemPauta)
    {
        $this->db->from('presenca');
        $this->db->where('id_item_pauta', $idItemPauta);
        $query = $this->db->get();

        $listPresenca = new ArrayObject();

        foreach ($query->result() as $row) {
            $presenca = new PresencaModel();

            $presenca->setIdItemPauta($row->id_item_pauta);
            $presenca->setStatusVoto($row->status_voto);
            $presenca->setIdUsuario($row->id_usuario);

            $listPresenca->append($presenca);
        }

        return $listPresenca;
    }

    public function selectByIdUsuario($idUsuario)
    {
        $this->db->from('presenca');
        $this->db->where('id_usuario', $idUsuario);
        $query = $this->db->get();

        foreach ($query->result() as $row) {
            $presenca = new PresencaModel();

            $presenca->setIdItemPauta($row->id_item_pauta);
            $presenca->setStatusVoto($row->status_voto);
            $presenca->setIdUsuario($row->id_usuario);

            $listPresenca->append($presenca);
        }

        return $listPresenca[0]->getIdItemPauta();
    }

    public function select($presenca)
    {
        $this->db->from('presenca');
        $this->db->where('id_usuario', $presenca->getIdUsuario());
        $this->db->where('id_item_pauta', $presenca->getIdItemPauta());
        $query = $this->db->get();

        $row = $query->row();

        if (isset($row)) {
            $presenca->setStatusVoto($row->status_voto);
        }

        return $presenca;
    }

    public function updateStatusVoto(PresencaModel $presenca)
    {
        $this->db->where('id_usuario', $presenca->getIdUsuario());
        $this->db->where('id_item_pauta', $presenca->getIdItemPauta());
        $this->db->set('status_voto', $presenca->getStatusVoto());
        $this->db->update('presenca');
    }

    public function deleteByIdItemPauta($IdItemPauta)
    {
        $this->db->where('id_item_pauta', $IdItemPauta);
        $this->db->delete('presenca');
    }

    public function deleteIntativos($IdItemPauta)
    {
        $this->db->where('id_item_pauta', $IdItemPauta);
        $this->db->where('status_voto', false);
        $this->db->delete('presenca');
    }

}
