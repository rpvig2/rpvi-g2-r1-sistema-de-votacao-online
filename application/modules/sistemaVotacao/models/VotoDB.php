<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VotoDB extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('VotoModel');        
        $this->load->database();
    }

    public function save($voto){
        $this->db->set('id_usuario', $voto->getIdUsuario());
        $this->db->set('id_opcao_voto', $voto->getIdOpcaoDeVoto());
        $this->db->set('id_item_pauta', $voto->getIdItemPauta());
        $this->db->insert('voto');
    }

    public function deleteAllByIdItemPauta($idItemPauta){        
        $this->db->where('id_item_pauta', $idItemPauta);
        $this->db->delete('voto');
    }

    public function selectByIdItemPauta($idItemPauta){
        $this->db->from('voto');
        $this->db->where('id_item_pauta', $idItemPauta);
        $query = $this->db->get();

        $listVotos = new ArrayObject();

        foreach($query->result() as $row){

            $voto = new VotoModel();

            $voto->setIdItemPauta($row->id_item_pauta);
            $voto->setIdOpcaoDeVoto($row->id_opcao_voto);
            $voto->setIdUsuario($row->id_usuario);

            $listVotos->append($voto);

        }

        return $listVotos;

    }

}