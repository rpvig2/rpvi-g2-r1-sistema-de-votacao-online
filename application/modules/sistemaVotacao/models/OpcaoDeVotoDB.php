<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OpcaoDeVotoDB extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OpcaoDeVotoModel');
        $this->load->model('ItemDePautaModel');
        $this->load->database();
    }

    public function selectByItemPautaId($Id)
    {
        $this->db->from('opcao_voto');
        $this->db->where('id_item_pauta', $Id);
        $query = $this->db->get();

        return $this->getData($query);
    }

    public function selectById($opcaoDeVotoId){
        $this->db->from('opcao_voto');
        $this->db->where('id_opcao_voto', $opcaoDeVotoId);
        $query = $this->db->get();

        return $this->getData($query)[0];
    }

    private function getData($query)
    {
        $listOpcaoDeVoto= new ArrayObject();

        foreach ($query->result() as $row) {
            $opcaoDeVoto = new OpcaoDeVotoModel();
            $opcaoDeVoto->setId($row->id_opcao_voto);
            $opcaoDeVoto->setDescricao(trim($this->getDescricao($row->id_descricao)));

            $listOpcaoDeVoto->append($opcaoDeVoto);
        }

        return $listOpcaoDeVoto;
    }

    public function save($arrayOptions, $itemId)
    {

        //salvando abstenção
        $data = array(
            'id_descricao' => 1,
            'id_item_pauta' => $itemId
        );
        $sql = $this->db->insert_string('opcao_voto', $data) . ' ON DUPLICATE KEY UPDATE id_opcao_voto=LAST_INSERT_ID(id_opcao_voto)';
        $this->db->query($sql);

        foreach ($arrayOptions as $opcaoDeVoto) {
            $id_descricao = $this->saveDescricao($opcaoDeVoto);

            $this->db->set('id_descricao', $id_descricao);
            $this->db->set('id_item_pauta', $itemId);
            $this->db->insert('opcao_voto');
        }
    }

    public function savePadrao($itemId)
    {
        for ($i = 1; $i < 4; $i++) {
            $this->db->set('id_descricao', $i);
            $this->db->set('id_item_pauta', $itemId);
            $this->db->insert('opcao_voto');
        }
    }

    private function saveDescricao($opcaoDeVoto)
    {
        $this->db->set('descricao', $opcaoDeVoto);
        $this->db->insert('descricao_opcao_voto');
        return $this->db->insert_id();
    }

    public function deleteByItemDePautaId($id)
    {
        $this->db->from('opcao_voto');
        $this->db->where('id_item_pauta', $id);
        $query = $this->db->get();

        foreach ($query->result() as $row) {
            $this->deleteByOpcaoVotoId($row->id_opcao_voto);
            $this->deleteDescricoes($row->id_descricao);
        }
    }

    private function deleteByOpcaoVotoId($id_opcao_voto)
    {
        $this->db->where('id_opcao_voto', $id_opcao_voto);
        $this->db->delete('opcao_voto');
    }

    private function deleteDescricoes($id_descricao)
    {
        if ($id_descricao > 3) {
            $this->db->where('id_descricao', $id_descricao);
            $this->db->delete('descricao_opcao_voto');
        }
    }


    private function getDescricao($id_descricao)
    {
        $this->db->from('descricao_opcao_voto');
        $this->db->where('id_descricao', $id_descricao);
        $query = $this->db->get();

        $row = $query->row();

        if (isset($row)) {
            return $row->descricao;
        }
    }
}
