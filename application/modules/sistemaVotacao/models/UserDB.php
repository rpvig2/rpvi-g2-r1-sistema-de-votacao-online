<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserDB extends CI_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->database();
        $this->load->model('UserModel');
    }

    public function selectByUserName($userName)
    {
        $this->db->from('usuario');
        $this->db->where('nome_usuario', $userName);
        $query = $this->db->get();
        
        return $this->getData($query);
        
    }

    public function selectById($id)
    {
        $this->db->from('usuario');
        $this->db->where('id_usuario', $id);
        $query = $this->db->get();

        return $this->getData($query);        
    }

    private function getData($query){
        
        $row = $query->row();

        $user = null;

        if (isset($row)) {
            $user = new UserModel();
            $user->setId($row->id_usuario);           
            $user->setUserName($row->nome_usuario);
            $user->setName($row->nome);
            $user->setPassword($row->senha);
        }

        return $user;
    }

}
