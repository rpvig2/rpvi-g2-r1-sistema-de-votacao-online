<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ReuniaoModel extends CI_Model
{
    private $id;
    private $name;
    private $data;
    private $colegiado;
    private $pauta;
    private $status;

    public function __construct()
    {
        parent::__construct();
        $this->pauta = new ArrayObject();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }
        
    public function getColegiado()
    {
        return $this->colegiado;
    }
    
    public function setColegiado($colegiado)
    {
        $this->colegiado = $colegiado;
    }
   
    public function getPauta()
    {
        return $this->pauta;
    }
    
    public function setPauta($pauta)
    {
        $this->pauta = $pauta;
    }
   
    public function isAberta()
    {
        return ($this->status == "ABERTA");
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
