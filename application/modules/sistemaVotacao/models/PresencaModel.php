<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PresencaModel extends CI_Model implements JsonSerializable
{

    private $idUsuario;   

    private $idItemPauta;

    private $statusVoto;

    private $usuario;
    
    public function __construct()
    {
        parent::__construct();        
    }

    public function jsonSerialize() {
        return ['idUsuario' => $this->idUsuario,
                'idItemPauta' => $this->idItemPauta,
                'statusVoto' => $this->statusVoto,
                'nome' => $this->usuario->getName()]; 
    }


    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    public function getIdItemPauta()
    {
        return $this->idItemPauta;
    }
 
    public function setIdItemPauta($idItemPauta)
    {
        $this->idItemPauta = $idItemPauta;
    }
  
    public function getStatusVoto()
    {
        return $this->statusVoto;
    }

    public function setStatusVoto($statusVoto)
    {
        $this->statusVoto = $statusVoto;
    }
  
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

    }

}