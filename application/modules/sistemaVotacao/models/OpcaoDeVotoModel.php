<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OpcaoDeVotoModel extends CI_Model
{

    private $id;
    private $descricao;
    private $quantidadeVotos;
    private $porcentagem;
    private $vencedor;

    public function __construct()
    {
        parent::__construct();
        $this->quantidadeVotos = 0;
        $this->vencedor = false;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }
    
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }  
    
    public function getQuantidadeVotos()
    {
        return $this->quantidadeVotos;
    }

    public function addVoto()
    {
        $this->quantidadeVotos = $this->quantidadeVotos+1;
    }

    
    public function getPorcentagem()
    {
        return $this->porcentagem;
    }
  
    public function setPorcentagem($quantidadeTotal)
    {
        if($quantidadeTotal == 0){
            $porcentagem = 0;
        }else{
            $porcentagem = ($this->quantidadeVotos/$quantidadeTotal) * 100;
        }        

        $this->porcentagem = $porcentagem;
    }
  
    public function getVencedor()
    {
        return $this->vencedor;
    }

    public function setVencedor($vencedor)
    {
        $this->vencedor = $vencedor;
    }
}
?>

    