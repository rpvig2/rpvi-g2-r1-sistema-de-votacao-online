<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ItemDePautaModel extends CI_Model
{
    private $id;
    private $descricao;
    private $opcaoDeVotoList;
    private $type;
    private $abertoParaVotacao;
    private $status;
    private $reuniaoId;

    public function __construct()
    {
        parent::__construct();
        $this->opcaoDeVotoList = new ArrayObject();
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setDescricao($descricao){
        $this->descricao = $descricao;
    }

    public function getOpcaoDeVotoList(){
        return $this->opcaoDeVotoList;
    }

    public function setOpcaoDeVotoList($opcaoDeVotoList){
        $this->opcaoDeVotoList = $opcaoDeVotoList;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }    
    
    public function getAbertoParaVotacao()
    {
        return ($this->status == "ABERTA");
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    public function getReuniaoId()
    {
        return $this->reuniaoId;
    }

    public function setReuniaoId($reuniaoId)
    {
        $this->reuniaoId = $reuniaoId;
    }
}

