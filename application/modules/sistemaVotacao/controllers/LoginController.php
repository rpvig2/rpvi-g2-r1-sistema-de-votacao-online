<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Cache-Control: no cache');
        
        $this->load->model('UserDB');
        $this->load->model('ReuniaoDB');
        $this->load->helper('url');
       
    }

    public function index()
    {
        if (! file_exists(APPPATH.'modules/sistemaVotacao/views/'.'loginView'.'.php')) {
            show_404();
        }       

        $this->load->view('loginView');
    }
    
    public function logar()
    {
        $userName = $this->input->post('inputUserName');
        $passWord =  $this->input->post('inputPassword');

        $userDB = new UserDB();

        $user = $userDB->selectByUserName($userName);

        if (isset($user)) {
            $this->redirectToDashBoard($user);
        } else {
            $erro = "Dados de usuário o senha inválidos!";
            $this->load->view('loginView');
        }
    }

    private function redirectToDashBoard($user)
    {
        $reuniaoDB = new ReuniaoDB();
        $reuniaoModeradorList = $reuniaoDB->selectAsModerador($user);
        $user->setReuniaoModeradorList($reuniaoModeradorList);
        $user->setReuniaoConselheiroList($reuniaoDB->selectAsConselheiro($user));
    
        if (! file_exists(APPPATH.'modules/sistemaVotacao/views/'.'dashboardView'.'.php')) {
            show_404();
        }

        $this->session->user = serialize($user);      
        $this->load->view('dashboardView');
    }
    
}
