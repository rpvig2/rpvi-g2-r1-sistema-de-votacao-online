<?php

defined('BASEPATH') or exit('No direct script access allowed');

class HomeController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Cache-Control: no cache');
        
        $this->load->helper('url');
    }

    public function index()
    {
        if (! file_exists(APPPATH.'modules/sistemaVotacao/views/'.'loginView'.'.php')) {
            show_404();
        }

        $this->load->view('homeView');
        
    }
}