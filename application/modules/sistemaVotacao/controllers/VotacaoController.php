<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VotacaoController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Cache-Control: no cache');
        
        $this->load->model('ItemDePautaDB');
        $this->load->model('ItemDePautaModel');
        $this->load->model('OpcaoDeVotoDB');
        $this->load->model('VotoDB');
        $this->load->model('VotoModel');        
        $this->load->model('UserModel');
        $this->load->model('PresencaDB');
        $this->load->model('PresencaModel'); 
        $this->load->helper('url');        
    }

    public function index()
    {
        $idReuniao = $this->input->post('reuniaoId');

        if(!isset($idReuniao)){
            $idReuniao = $this->session->reuniaoId;
        }

        $data['reuniaoId'] = $idReuniao;
        $response = $this->entrarVotacao();

        if(!isset($response)){           
            $response = $this->load->view('esperaVotacaoView',$data);
        }
        
        echo $response;
    }

    public function verificarVotacaoAberta(){
        
        $response = $this->entrarVotacao();
       
        if(!isset($response)){
            $this->output->set_header('HTTP/1.0 404');                      
        }

        echo $response;

    }

    private function entrarVotacao(){

        $reuniaoId = $this->input->post('reuniaoId');
        $data = new ArrayObject();
        $response = null;

        $itemDePautaDB = new ItemDePautaDB();       

        $pauta = $itemDePautaDB->selectByReuniaoId($reuniaoId);       
        $data['reuniaoId'] = $reuniaoId;   

        foreach($pauta as $itemDePauta){
            if($itemDePauta->getAbertoParaVotacao()){                
                $data['itemDePauta'] = $itemDePauta;                
                
                if($this->adicionarPresenca($itemDePauta)){
                    $this->output->set_header('HTTP/1.0 200 OK');
                    $response= $this->load->view('votacaoView',$data, TRUE);
                    break;
                }

            }                   
        }        

        return $response;

    }

    private function adicionarPresenca($itemDePauta){

        $usuario = unserialize($this->session->user);

        $presenca = new PresencaModel();
        $presenca->setIdItemPauta($itemDePauta->getId());
        $presenca->setIdUsuario($usuario->getId());

        $presencaExistente = $this->PresencaDB->select($presenca);
        
        if(isset($presencaExistente)){

            if($presencaExistente->getStatusVoto()){
                return false;
            }

        }else{
            return false;
        }

        $presenca->setStatusVoto(false);

        $this->PresencaDB->save($presenca);

        return true;

    }

    public function salvarVoto(){
        $idOpcaoDeVoto = $this->input->post('opcaoDeVotoId');
        $itemPautaId = $this->input->post('itemPautaId');        
        $user = unserialize($this->session->user);

        $voto = new VotoModel();
        $voto->setIdOpcaoDeVoto($idOpcaoDeVoto);
        $voto->setIdUsuario($user->getId());
        $voto->setIdItemPauta($itemPautaId);

        $votoDB = new VotoDB();  
        $votoDB->save($voto);

        $presenca = new PresencaModel();
        $presenca->setIdItemPauta($itemPautaId);
        $presenca->setIdUsuario($user->getId());
        $presenca->setStatusVoto(true);

        $this->PresencaDB->updateStatusVoto($presenca);

        $itemPauta = $this->ItemDePautaDB->selectById($itemPautaId);

        $this->session->reuniaoId = $itemPauta->getReuniaoId();

        echo base_url('votacao');

    }

    public function sairVotacao(){

        $itemPautaId = $this->input->post('itemPautaId');
        $user = unserialize($this->session->user);
        $presenca = new PresencaModel();
        $presenca->setIdItemPauta($itemPautaId);
        $presenca->setIdUsuario($user->getId());       
    
        $presenca = $this->PresencaDB->select($presenca);

        if(!$presenca->getStatusVoto()){
            $this->PresencaDB->delete($presenca);
        }

    }

}
