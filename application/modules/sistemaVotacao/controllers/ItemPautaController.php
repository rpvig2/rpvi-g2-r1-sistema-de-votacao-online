<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ItemPautaController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Cache-Control: no cache');
        
        $this->load->helper('url');
        $this->load->model('ItemDePautaDB');
        $this->load->model('ItemDePautaModel');
        $this->load->model('OpcaoDeVotoDB');
        $this->load->model('VotoDB');
        $this->load->model('PresencaModel');
        $this->load->model('PresencaDB');
        $this->load->model('UserDB');
        $this->load->model('VotacaoModel');
    }

    public function index()
    {
    }

    public function saveOptions()
    {
        $arrayOptions = $this->input->post('arrayOptions');
        $idItem = $this->input->post('idItem');
        $tipoVotacao = $this->input->post('tipoVotacao');

        $itemDePautaDB = new ItemDePautaDB();
        $opcaoDeVotoDB = new OpcaoDeVotoDB();

        $opcaoDeVotoDB->deleteByItemDePautaId($idItem);

        if ($tipoVotacao == 'PADRAO') {
            $itemDePautaDB->updateVotacaoType($idItem, 0);
            $opcaoDeVotoDB->savePadrao($idItem);
        } else {
            $itemDePautaDB->updateVotacaoType($idItem, 1);
            $arrayOptions = explode(",", $arrayOptions);
            $opcaoDeVotoDB->save($arrayOptions, $idItem);
        }
    }

    public function abrirVotacao()
    {
        $idItem = $this->input->post('idItem');
        $reuniaoId = $this->input->post('reuniaoId');
        $itemDePautaDB = new ItemDePautaDB();

        $itemDePautaAberto = $itemDePautaDB->getItemPautaAbertoByReuniaoId($reuniaoId);

        if (isset($itemDePautaAberto)) {
            $this->output->set_header('HTTP/1.0 404');
            $response = $itemDePautaAberto->getDescricao();
            echo $response;
        } else {
            $itemDePautaDB->updateStatus($idItem, "ABERTA");
            $presencaDB = new PresencaDB();
            $data['listPresenca'] = $presencaDB->selectByIdItemPauta($idItem);
            $data['itemDePauta'] = $itemDePautaDB->selectById($idItem);
            $response = $this->load->view('itemPautaEmVotacaoView', $data);
            echo $response;
        }
    }

    public function finalizarVotacao($idItem)
    {
        if (!isset($idItem)) {
            $idItem = $this->input->post('idItem');
        }
        
        $data = new ArrayObject();

        //verificar se deu empate
        $itemDePautaDB = new ItemDePautaDB();
        $itemDePauta = $itemDePautaDB->selectById($idItem);
        
        $votacaoModel = new VotacaoModel();
        if ($votacaoModel->calcularResultado($itemDePauta, $data)) {
            $itemDePautaDB->updateStatus($idItem, "FINALIZADA");
            $itemDePauta->setStatus("FINALIZADA");
            $data['itemDePauta'] = $itemDePauta;
            $this->output->set_status_header(202);
            echo $this->load->view('itemPautaEmVotacaoView', $data);
        } else {
            $this->output->set_status_header(404);
        }
    }

    public function cancelarVotacao()
    {
        $idItem = $this->input->post('idItem');
        $itemDePautaDB = new ItemDePautaDB();
        $itemDePautaDB->updateStatus($idItem, "FECHADA");

        $itemDePauta = $itemDePautaDB->selectById($idItem);

        $votoDB = new VotoDB();
        $votoDB->deleteAllByIdItemPauta($idItem);
  
        $this->PresencaDB->deleteByIdItemPauta($idItem);

        $this->session->itemPautaId = $idItem;
        echo redirect('itemDePauta');
    }

    public function voltarPauta()
    {
        $reuniaoId = $this->input->post('reuniaoId');
        $this->session->reuniaoId = $reuniaoId;
        echo base_url('pauta');
    }

    public function verificarFimVotacao()
    {
        $corum = 2;
        $votoPendente = false;

        $idItem = $this->input->post('itemId');
        $forcarFim = $this->input->post('forcarFim');

        $presencaDB = new PresencaDB();
        $listPresenca = $presencaDB->selectByIdItemPauta($idItem);
             
        if (isset($forcarFim)) {

            $votoDB = new VotoDB();
            $listaVotos = $votoDB->selectByIdItemPauta($idItem);
            
            if ($listaVotos->count() >= $corum) {
                $presencaDB->deleteIntativos($idItem);
            }

        }else{            

            if ($listPresenca->count() >= $corum) {
                foreach ($listPresenca as $voto) {
                    if (!$voto->getStatusVoto()) {
                        $votoPendente = true;
                        break;
                    }
                }
            }
        }

        foreach ($listPresenca as $conselheiro) {
            $conselheiro->setUsuario($this->UserDB->selectById($conselheiro->getIdUsuario()));
        }

        if (!$votoPendente && $listPresenca->count() >= $corum) {
            $this->finalizarVotacao($idItem);
        } else {
            echo json_encode($listPresenca, JSON_PRETTY_PRINT);
        }
    }
}
