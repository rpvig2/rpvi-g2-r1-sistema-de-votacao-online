<?php

defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Cache-Control: no cache');
        
        $this->load->helper('url');
        $this->load->model('ItemDePautaDB');
        $this->load->model('ReuniaoDB');
        $this->load->model('ReuniaoModel');
        $this->load->model('ItemDePautaModel');
        $this->load->model('VotoDB');
        $this->load->model('VotoModel');
        $this->load->model('VotacaoModel');
        $this->load->model('PresencaModel');
        $this->load->model('PresencaDB');
        $this->load->model('UserDB');
        
    }

    public function abrirReuniao()
    {

        $reuniaoId = $this->input->post('reuniaoId');
        
        //Se o dado vier de um redirect
        if (!isset($reuniaoId)) {
            $reuniaoId = $this->session->reuniaoId;
        }

        $itemDePautaDB = new ItemDePautaDB();
        $reuniaoDB = new ReuniaoDB();

        $pauta = $itemDePautaDB->selectByReuniaoId($reuniaoId);
        $reuniao = $reuniaoDB->getById($reuniaoId);

        $data = new ArrayObject();
        $data['pauta'] = $pauta;
        $data['reuniao'] = $reuniao;
        $this->load->view('pautaView', $data);
    }

    public function abrirItemDePauta()
    {

        $itemPautaId = $this->input->post('itemPautaId');

        //Se o dado vier de um redirect
        if (!isset($itemPautaId)){
            $itemPautaId = $this->session->itemPautaId;
        } 

        $itemDePautaDB = new ItemDePautaDB();

        $itemDePauta = $itemDePautaDB->selectById($itemPautaId);

        $data = new ArrayObject();
        $data['itemDePauta'] = $itemDePauta;
        
        //Verifica se já está finalizada ou aberta e redireciona para tela correta
        if ($itemDePauta->getStatus() === "FINALIZADA") {

            $votacaoModel = new VotacaoModel();
            
            $data['empate'] = !$votacaoModel->calcularResultado($itemDePauta, $data);
           
            $this->load->view('itemPautaEmVotacaoView', $data);

        } elseif ($itemDePauta->getStatus() === "ABERTA") {

            $presencaDB = new PresencaDB();    

            $listPresenca = $presencaDB->selectByIdItemPauta($itemDePauta->getId());

            foreach($listPresenca as $conselheiro){
                $conselheiro->setUsuario($this->UserDB->selectById($conselheiro->getIdUsuario()));
            }

            $data['listPresenca'] = $listPresenca;
            $this->load->view('itemPautaEmVotacaoView', $data);
        } else {
            //Para deixar em branco o padrão que já é preenchido com html
            if ($itemDePauta->getType() == "PADRAO") {
                $itemDePauta->setOpcaoDeVotoList(new ArrayObject());
            }

            $this->load->view('itemPautaView', $data);
        }
    }

    public function atualizarStatusReuniao()
    {
        $idReuniao = $this->input->post('idReuniao');
        $status = $this->input->post('status');

        $reuniaoDB = new ReuniaoDB();

        if (intval($status)) {
            $reuniaoDB->updateAberta(intval($idReuniao), "ABERTA");
        } else {
            $reuniaoDB->updateAberta(intval($idReuniao), "FECHADA");
        }
    }

    public static function verificarReunioesAbertas()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        
        $reuniaoDB = new ReuniaoDB();
        $reunioes = $reuniaoDB->selectAll();
        
        $echo = "{\ndata:";
        
        for ($i=0; $i < count($reunioes); $i++) {
            if ($reunioes[$i]->isAberta()) {
                if ($i == count($reunioes) - 1) {
                    $echo = $echo."id: ".$reunioes[$i]->getId()."\n";
                } else {
                    $echo = $echo."id: ".$reunioes[$i]->getId().",";
                }
            }
        }
        
        $echo = $echo."}\n\n";

        echo $echo;
        flush();
    }
}
