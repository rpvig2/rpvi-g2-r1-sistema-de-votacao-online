<?php
    defined('BASEPATH') or exit('No direct script access allowed');
		
	$data['title'] = "Item de Pauta";
    $this->load->view('templates/header', $data);
?>

<script src="<?php echo base_url('/application/modules/sistemaVotacao/views/public/js/itemPauta.js')?>"></script>
</head>
<body>
<div class="container">  
	<div id="body">	

		<input id="reuniaoId" type="hidden" value="<?php echo $itemDePauta->getReuniaoId()?>"/>
    	<div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
        	<div class="lh-100">
				<h3 id='itemPauta' class="mb-0 text-white lh-100" value=<?php echo $itemDePauta->getId() ?>>Item de Pauta: <?php echo $itemDePauta->getDescricao() ?></h1>
			</div>    
        </div>

		
		<label class="switch">
			<input id='customActived' class="switch-input" type="checkbox" />
			<span class="switch-label" data-on="Customizada" data-off="Padrão"></span> 
			<span class="switch-handle"></span> 
		</label>
		
		<?php if ($itemDePauta->getType() == 'CUSTOMIZADA'):?>
			<script>
				document.getElementById('customActived').checked = true;
			</script>			
		<?php else:?>
			<script>
				document.getElementById('customActived').checked = false;
			</script>	
		<?php endif?>
		
		<button type='optional' style='float:right'  name='customButton' class="btn btn-unipampa" onclick="insertVotingOption()">Adicionar</button>
		<input type='optional' class="form-control" style='float:right; margin-right: 10px; width: 25%'  name='customField' id='customField' placeholder='Insira uma Opção'>
		<br/><br/>

		<table id='customTable' name='customTable' class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="8%">Ordenação</th>
					<th >Descrição da Opção</th>
					<th width="6%">Ação</th>
				</tr>
			</thead>
			<tbody id = 'tableBody'>
				<tr>
					<td></td>
					<td>Abstenção</td>
					<td></td>
				</tr>

				<?php for ($i = 1; $i < count($itemDePauta->getOpcaoDeVotoList()) ; ++$i) : ?>					
				<tr name='<?php echo $i ?>'>
					<td>
						<button name='buttons' value='<?php echo $i ?>' class='btn btn-unipampa' onclick='upOption(<?php echo $i ?>)'>
							<i class="fas fa-angle-up"></i>
						</button>
						<button name='buttons' value='<?php echo $i ?>' class='btn btn-unipampa' onclick='downOption(<?php echo $i ?>)'>
							<i class="fas fa-angle-down"></i>
						</button>
					</td>
					<td name='arrayVotingOptions'  value='<?php echo $i ?>'>					
						<?php echo $itemDePauta->getOpcaoDeVotoList()[$i]->getDescricao()?>
					</td>
					<td>
						<button name='buttons' value='<?php echo $i ?>' class='btn btn-danger' onclick='deleteVotingOption(<?php echo $i ?>)'>
							<i class="fas fa-trash"></i>
						</button>
					</td>
				</tr>
				<?php endfor; ?>
			</tbody>
		</table>

		<table id='defaultTable' name='defaultTable' class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Descrição da Opção</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Abstenção</td>
				</tr>
				<tr>
					<td>Favorável</td>
				</tr>
				<tr>
					<td>Contrário</td>
				</tr>
			</tbody>
		</table>

		<div class="row">
			<div class="col">
				<button class='btn btn-unipampa' title='Voltar' onclick="showModalBackPage()">
					<i class="fas fa-undo"></i>
				</button>
			</div>
			<div class="col" style="text-align:center">
				<button class='btn btn-unipampa ' title='Iniciar Votacão'  onclick='verifySave("e enviar para votação", "saveOptions(true)")'>
					<i class="fas fa-check"></i>
					<b>Iniciar Votação</b>
				</button>
			</div>
			<div class="col">
				<button class='btn btn-unipampa' style="float:right" title='Salvar'  onclick='verifySave("", "saveOptions(false)")'>
					<i class="fas fa-save"></i>
				</button>
			</div>
		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="titleModalLabel">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="titleModalLabel">Informações</h4>
					</div>
				<div class="modal-body">
					
				</div>
					<div class="modal-footer">
						<button id='dismiss' type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button id='verifyBack' type="button" class="btn btn-default" onclick='backPage()'>Sim</button>
						<button id='completeAction' type="button" class="btn btn-unipampa">Sim</button>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('templates/footer');?>