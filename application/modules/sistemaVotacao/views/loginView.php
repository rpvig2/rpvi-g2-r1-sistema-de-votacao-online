<?php 
$data['title'] = "Login";
$this->load->view('templates/header', $data);
?>
</head>
<body>
<div class="container">  

<form id="loginForm" class="form-login text-center" action="<?php echo base_url('dashboard')?>" method="POST">      
  <h1 class="h3 mb-3 font-weight-normal">Login Guri</h1>
  <label for="inputEmail" class="sr-only">Usuario</label>
  <input type="text" name="inputUserName" id="inputUserName" class="form-control" placeholder="Usuario" required autofocus>
  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Senha" required>      
  <button class="btn btn-lg btn-block" type="submit">Entrar</button>      
</form>
  
<?php if (isset($erro)):?>
  <div class="alert alert-danger">
    <strong>Dados Inválidos!</strong> <?php echo $erro ?>
  </div>
<?php endif?>

<?php $this->load->view('templates/footer');?>