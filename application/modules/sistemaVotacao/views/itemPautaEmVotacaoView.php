<?php
    defined('BASEPATH') or exit('No direct script access allowed');
    $data['title'] = "Item de Pauta - Votação";
    $this->load->view('templates/header', $data);
?>
		
	<script src="<?php echo base_url('/application/modules/sistemaVotacao/views/public/js/itemPautaEmVotacao.js')?>"></script>
	<script src="<?php echo base_url('/application/modules/sistemaVotacao/views/public/js/verificarPresenca.js')?>"></script>
	
</head>
<body>
    <div class="container">
		
		<input id="reuniaoId" type="hidden" value="<?php echo $itemDePauta->getReuniaoId()?>"/>
		<input id="itemPautaId" type="hidden" value="<?php echo $itemDePauta->getId()?>"/>
        <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
            <div class="lh-100">
                <h3 id='itemPauta' class="mb-0 text-white lh-100" value=<?php echo $itemDePauta->getId(); ?>>Item de Pauta: <?php echo $itemDePauta->getDescricao(); ?></h1>
            </div>    
        </div>

        <table id='customTable' name='customTable' class="table table-striped table-bordered">
            <thead>
                <tr>					
                    <th scope="col" width="6%">id</th>	
					<th scope="col">Descrição da Opção</th>
					<?php if($itemDePauta->getStatus() == "FINALIZADA"):?>
						<th scope="col" width="20%">Quantidade Votos</th>	
						<th scope="col" width="20%">Porcentagem Votos</th>
					<?php endif?>				
                </tr>
            </thead>
            <tbody id = 'tableBody'>				
                <?php foreach ($itemDePauta->getOpcaoDeVotoList() as $opcaoDeVoto) : ?>
					<?php if($opcaoDeVoto->getVencedor()){
						$class = "table-success";
					}else{
						$class = "";
					}?>				
                <tr scope="row" id='<?php echo $opcaoDeVoto->getId(); ?>' class='<?php echo $class?>'>
                    <td>					
                        <?php echo $opcaoDeVoto->getId();?>
                    </td>					
                    <td>					
                        <?php echo $opcaoDeVoto->getDescricao();?>
					</td>
					<?php if($itemDePauta->getStatus() == "FINALIZADA"):?>
						<td>					
							<?php echo $opcaoDeVoto->getQuantidadeVotos();?>
						</td>					
						<td>					
							<?php echo $opcaoDeVoto->getPorcentagem();?>%
						</td>
					<?php endif?>					
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
		
       
		<?php if($itemDePauta->getStatus() == "ABERTA"):?>
			<div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
				<div class="lh-100">
					<h3>Votos Pendentes</h1>
				</div>
			</div>
			<table id='logadosTable' name='logadosTable' class="table table-striped table-bordered">
				<thead>
					<tr>					
						<th scope="col" width="6%">id</th>	
						<th scope="col">Nome</th>								
					</tr>
				</thead>
				<tbody id ='tableLogadosBody'>				
					<?php foreach ($listPresenca as $conselheiro) : ?>
						<?php if (!$conselheiro->getStatusVoto()) : ?>
							<tr scope="row" id='<?php echo $conselheiro->getIdUsuario(); ?>'>
								<td>					
									<?php echo $conselheiro->getIdUsuario();?>
								</td>					
								<td>					
									<?php echo $conselheiro->getUsuario()->getName();?>
								</td>								
							</tr>
						<?php endif?>
					<?php endforeach; ?>
				</tbody>
        	</table>
		<?php endif?>

		<?php if($itemDePauta->getStatus() == "FINALIZADA"):?>
		<div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
				<div class="lh-100">
					<h3>Votos Nominais</h1>
				</div>
			</div>	
		<table id='votosTable' name='votosTable' class="table table-striped table-bordered">
            <thead>
                <tr>					
                    <th scope="col">Nome Conselheiro</th>	
					<th scope="col">Voto</th>
					<th scope="col" width="6%">Id</th>							
                </tr>
            </thead>
            <tbody id = 'tableBody'>				
                <?php foreach ($listVotos as $voto) : ?>					
                <tr scope="row" id='<?php echo $opcaoDeVoto->getId(); ?>'>
                    <td>					
                        <?php echo $voto->getUsuario()->getName();?>
                    </td>					
                    <td>					
                        <?php echo $voto->getOpcaoDeVoto()->getDescricao();?>
					</td>		
					<td>					
                        <?php echo $voto->getIdOpcaoDeVoto();?>
					</td>		
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
		<?php endif?>

        <div class="row">			
            <div class="col">
				<?php if($itemDePauta->getStatus() == "ABERTA"):?>
					<button class='btn btn-danger' title='Cancelar' 
					onclick="showModalCompleteAction('Deseja cancelar a votação e voltar a tela de ediçao de item de pauta?', 'cancelarVotacao()')">
						<i class="fas fa-ban"></i>
						<b>Cancelar</b>
					</button>				
				<?php else:?>
					<button class='btn btn-unipampa' title='Voltar' 
					onclick="showModalCompleteAction('Deseja voltar para tela de pauta?', 'voltar()')">
						<i class="fas fa-undo"></i>
						<b>Voltar</b>
					</button>
				<?php endif?>
			</div>
            <div class="col">
            </div>
            <div class="col">
				<?php if($itemDePauta->getStatus() == "ABERTA"):?>
					<button class='btn btn-unipampa float-right' title='Finalizar' 
					onclick="showModalCompleteAction('Deseja finalizar a votação e ver o resultado?', 'finalizarVotacao()')">
						<i class="fas fa-check"></i>
						<b>Finalizar</b>
					</button>
				<?php endif?>
            </div>
        </div>	

        <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="titleModalLabel">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="titleModalLabel">Confirmação</h4>
					</div>
				<div class="modal-body">
					
				</div>
					<div class="modal-footer">
						<button id='dismiss' type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>						
						<button id='completeAction' type="button" class="btn btn-unipampa">Sim</button>
					</div>
				</div>
			</div>
		</div>    

		<?php if($itemDePauta->getStatus() == "ABERTA"):?>
			<script>
				!verificarPresenca();
			</script>
		<?php endif ?>

		<?php if(isset($empate)):?>
			<?php if($empate):?>
				<script>
					showModalCancelarAction();
				</script>
			<?php endif ?>
		<?php endif ?>
<?php $this->load->view('templates/footer');?>