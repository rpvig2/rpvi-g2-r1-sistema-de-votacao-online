<?php
$data['title'] = "Dashboard";
$this->load->view('templates/header', $data);
?>
<?php 
  $data['idTable'] = ["moderadorTable","conselheiroTable"];
  $this->load->view('templates/dataTable', $data);?>

  <script src="<?php echo base_url('/application/modules/sistemaVotacao/views/public/js/dashboard.js')?>"></script>
</head>
<body>
<div class="container">  

<?php
  if (isset($this->session->user)) {
      $user = unserialize($this->session->user);
  } else {
      $this->load->view('login');
  }
?>

<?php if ($user->getReuniaoModeradorList()->count() > 0):?>
  <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
      <div class="lh-100">
          <h1 class="mb-0 text-white lh-100">Reuniões como Moderador</h1>        
      </div>
  </div>
  <table class="table" id="moderadorTable">
      <thead>
        <tr>        
          <th scope="col" >Nome</th>
          <th scope="col" >Data</th>
          <th scope="col">Ação</th>
        </tr>
      </thead>  
      <tbody>   
        <?php foreach ($user->getReuniaoModeradorList() as $reuniao): ?>   
          <tr>      
            <td> <?php echo $reuniao->getName()?> </td>
            <td> <?php echo $reuniao->getData()?> </td>
            <td> 
              <form method="POST" action="<?php echo base_url('pauta')?> ">
                <button class="btn btn-lg" type="submit">Moderar</button>
                <input type="hidden" name="reuniaoId" value="<?php echo $reuniao->getId(); ?>"/>
              </form>
            </td>     
          </tr>
          <?php endforeach?>
      </tbody>
  </table>
<?php endif ?>

<?php if ($user->getReuniaoConselheiroList()->count() > 0):?>
  <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
      <div class="lh-100">
          <h1 class="mb-0 text-white lh-100">Reuniões como Conselheiro</h1>        
      </div>
  </div>
  <table class="table" id="conselheiroTable" >
      <thead>
        <tr>        
          <th scope="col" >Nome</th>
          <th scope="col" >Data</th>
          <th scope="col">Ação</th>
        </tr>
      </thead>  
      <tbody>   
        <?php foreach ($user->getReuniaoConselheiroList() as $reuniao): ?>   
          <tr>      
            <td> <?php echo $reuniao->getName()?> </td>
            <td> <?php echo $reuniao->getData()?> </td>
            <td>
            <form method="POST" action="<?php echo base_url('votacao')?> ">
              <button id=<?php echo $reuniao->getId()?> name="entrarReuniao" class="btn btn-lg" type="submit">Entrar</button>
              <input type="hidden" name="reuniaoId" value="<?php echo $reuniao->getId(); ?>"/>
            </form>
            <?php if ($reuniao->isAberta()):?>
              <script>
                document.getElementById(<?php echo $reuniao->getId()?>).disabled = false;
              </script>
            <?php else: ?>
              <script>
                document.getElementById(<?php echo $reuniao->getId()?>).disabled = true;
              </script>
            <?php endif ?>
            </td>     
          </tr>
          <?php endforeach?>
      </tbody>
  </table>
<?php endif ?>
<?php $this->load->view('templates/footer');?>
