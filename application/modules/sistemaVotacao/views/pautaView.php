<?php
    defined('BASEPATH') or exit('No direct script access allowed');
    $data['title'] = "Pauta";
    $this->load->model('ItemDePautaModel');
    $this->load->view('templates/header', $data);
?>

<?php 
$data['idTable'] = ["itemPautaTable"];
$this->load->view('templates/dataTable', $data);
?>

<script type="text/javascript" src="<?php echo base_url('/application/modules/sistemaVotacao/views/public/js/pauta.js')?>"></script>

</head>
<body>
    <div class="container"> 
    <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
        <div class="lh-100">
            <h1 class="mb-0 text-white lh-100">Pauta</h1>        
        </div>
    </div>
    <b>Abrir Reunião</b>

    <label id=<?php echo $reuniao->getId() ?> name="idReuniao" class="switch-reuniao">
        <input id='customActived' class="switch-input" type="checkbox" />
	    <span class="switch-label-reuniao" data-on="Aberta" data-off="Fechada"></span> 
	    <span class="switch-handle-reuniao"></span> 
    </label>

    <?php if ($reuniao->isAberta()):?>
        <script>
			document.getElementById('customActived').checked = true;
		</script>			
	<?php else:?>
        <script>
			document.getElementById('customActived').checked = false;
		</script>			
	<?php endif?>

<table class="table">
    <thead>
    <tr>        
        <th scope="col">Nome</th>
        <th scope="col">Status</th>        
        <th scope="col">Ação</th>
    </tr>
    </thead>  
    <tbody>
        <?php foreach ($pauta as $itemDePauta):?>   
            <tr>    
                <td><?php echo $itemDePauta->getDescricao()?></td>
                <td><?php echo $itemDePauta->getStatus()?></td>  
                <td>
                    <form method="POST" action="<?php echo base_url('itemDePauta')?>">
                        <button class="btn btn-lg" type="submit">Entrar</button>
                        <input type="hidden" name="itemPautaId" value="<?php echo $itemDePauta->getId(); ?>"/>
                    </form>
                </td>         
            </tr>
        <?php endforeach ?>
    </tbody>
    <tr>
        <th scope='col'>Conselheiros Logados</th>
    </tr>
</table>
<?php $this->load->view('templates/footer');?>