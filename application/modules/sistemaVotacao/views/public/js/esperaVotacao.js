function verificarVotacao() {
    var reuniaoId = document.getElementById('reuniaoId');

    var request = new XMLHttpRequest();
    var data = new FormData();
    request.open('POST', 'verificarVotacaoAberta', true);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.open();
            document.write(this.responseText);
            document.close();
        }
    };

    data.append('reuniaoId', reuniaoId.value);
    request.send(data);
    setTimeout(verificarVotacao, 1000);

};