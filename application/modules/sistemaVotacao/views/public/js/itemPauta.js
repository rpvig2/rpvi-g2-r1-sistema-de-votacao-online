$(document).ready(function () {
    var element = document.getElementById('customActived');
    configureFieldAdd(element.checked);
    configureTables(element.checked);

    $('#customActived').change(function () {
        var element = document.getElementById('customActived');
        configureFieldAdd(element.checked);
        configureTables(element.checked);
    });
});



function configureFieldAdd(isCustom) {
    if (isCustom && !($('[name="customField"]').is(':visible'))) {
        $('[name="customField"]').toggle(0);
    } else if (!isCustom && $('[name="customField"]').is(':visible')) {
        $('[name="customField"]').toggle(0);
    }

    if (isCustom && !($('[name="customButton"]').is(':visible'))) {
        $('[name="customButton"]').toggle(0);
    } else if (!isCustom && $('[name="customButton"]').is(':visible')) {
        $('[name="customButton"]').toggle(0);
    }
}

function configureTables(isCustom) {
    if (isCustom && !($('[name="customTable"]').is(':visible'))) {
        $('[name="customTable"]').toggle(0);
        $('[name="defaultTable"]').toggle(0);
    } else if (!isCustom && $('[name="customTable"]').is(':visible')) {
        $('[name="customTable"]').toggle(0);
        $('[name="defaultTable"]').toggle(0);
    }

    if (!isCustom && !($('[name="defaultTable"]').is(':visible'))) {
        $('[name="defaultTable"]').toggle(0);
    } else if (isCustom && $('[name="defaultTable"]').is(':visible')) {
        $('[name="defaultTable"]').toggle(0);
    }
}

function getNextNumberElement() {
    var options = document.getElementsByName('buttons');
    if (options.length > 0) {
        buttonElement = options[options.length - 1].value;
        var elementNumber = Number(buttonElement);
        return ++elementNumber;
    } else {
        return 0;
    }
}

function getDeleteButton(elementNumber) {
    var button = document.createElement('button');

    button.setAttribute('name', 'buttons');
    button.setAttribute('value', elementNumber);
    button.setAttribute('class', 'btn btn-danger');
    button.setAttribute('onclick', "deleteVotingOption(" + elementNumber + ")");
    button.innerHTML = '<i class=\"fas fa-trash\"></i>';

    return button;
}

function getAngleUpButton(elementNumber) {
    var button = document.createElement('button');

    button.setAttribute('name', 'buttons');
    button.setAttribute('value', elementNumber);
    button.setAttribute('class', 'btn btn-unipampa');
    button.setAttribute('style', 'margin-right:4px');
    button.setAttribute('onclick', "upOption(" + elementNumber + ")");
    button.innerHTML = '<i class=\"fas fa-angle-up\"></i>';

    return button;
}

function getAngleDownButton(elementNumber) {
    var button = document.createElement('button');

    button.setAttribute('name', 'buttons');
    button.setAttribute('value', elementNumber);
    button.setAttribute('class', 'btn btn-unipampa');
    button.setAttribute('onclick', "downOption(" + elementNumber + ")");
    button.innerHTML = '<i class=\"fas fa-angle-down\"></i>';

    return button;
}

function upOption(elementNumber) {
    var arrayOptions = document.getElementsByName('arrayVotingOptions');
    for (let index = 0; index < arrayOptions.length; index++) {
        if (arrayOptions[index].getAttribute('value') == elementNumber) {
            if (index == 0) {
                showModalMessage('Não é possível subir mais a opção !');
            } else {
                var swap = arrayOptions[index - 1].innerHTML;
                arrayOptions[index - 1].innerHTML = arrayOptions[index].innerHTML;
                arrayOptions[index].innerHTML = swap;
            }
        }
    }
}

function downOption(elementNumber) {
    var arrayOptions = document.getElementsByName('arrayVotingOptions');
    for (let index = 0; index < arrayOptions.length; index++) {
        if (arrayOptions[index].getAttribute('value') == elementNumber) {
            if (index == arrayOptions.length - 1) {
                showModalMessage('Não é possível descer mais a opção !');
            } else {
                var swap = arrayOptions[index + 1].innerHTML;
                arrayOptions[index + 1].innerHTML = arrayOptions[index].innerHTML;
                arrayOptions[index].innerHTML = swap;
            }
        }
    }
}

function getTableTd(type, votingOption, elementNumber) {
    var tableTd = document.createElement('td');

    if (type == 'option') {
        tableTd = document.createElement('td');
        tableTd.setAttribute('name', 'arrayVotingOptions');
        tableTd.setAttribute('value', elementNumber);
        tableTd.innerHTML = votingOption;
    }

    return tableTd;
}

function clearField(idField) {
    document.getElementById(idField).value = '';
}

function appendChilds(fatherChild, childChild) {
    fatherChild.appendChild(childChild);
    return fatherChild;
}

function createTr(elementNumber) {
    var tr = document.createElement('tr');
    tr.setAttribute('name', elementNumber);
    return tr;
}

function backPage() {
    javascript: history.back();
}

function verifyDuplicateOption(votingOption) {
    var optionsExistents = document.getElementsByName('arrayVotingOptions');
    for (let index = 0; index < optionsExistents.length; index++) {
        if (optionsExistents[index].innerHTML == votingOption)
            return true;
    }
    return false;
}

function insertVotingOption() {
    var elementNumber = getNextNumberElement();
    var votingOption = document.getElementById('customField').value;
    if (!(verifyDuplicateOption(votingOption))) {
        if (votingOption == '') {
            showModalMessage('É preciso inserir algum valor para adicionar opção de voto');
        } else {
            var tableBody = document.getElementById('tableBody');
            var tr = createTr(elementNumber);
            var tdOption = getTableTd('option', votingOption, elementNumber);
            var tdAction = getTableTd('action', votingOption, 0);
            var tdOrder = getTableTd('order', votingOption, 0);
            var button = getDeleteButton(elementNumber);

            var buttonUp = getAngleUpButton(elementNumber);
            var buttonDown = getAngleDownButton(elementNumber);

            tdAction = appendChilds(tdAction, button);
            tdOrder = appendChilds(tdOrder, buttonUp);
            tdOrder = appendChilds(tdOrder, buttonDown);

            tr = appendChilds(tr, tdOrder);
            tr = appendChilds(tr, tdOption);
            tr = appendChilds(tr, tdAction);
            tableBody = appendChilds(tableBody, tr);

            clearField('customField');
        }
    } else {
        showModalMessage('Opção já cadastrada, verifique o dado informado');
    }
}

function deleteVotingOption(valueButton) {
    var options = document.getElementsByName(valueButton);
    for (i = 0; i < options.length; i++) {
        options[i].parentNode.removeChild(options[i]);
    }
}

function showModalMessage(message) {
    $('#myModal .modal-body').text(message);
    $('#myModal').modal('show');
    document.getElementById('completeAction').style.display = 'none';
    document.getElementById('verifyBack').style.display = 'none';

    document.getElementById('dismiss').innerHTML = 'Fechar';
    document.getElementById('dismiss').style.display = 'block';
}

function showModalCompleteAction(message, action) {
    $('#myModal .modal-body').text(message);
    $('#myModal').modal('show');
    document.getElementById('verifyBack').style.display = 'none';

    document.getElementById('dismiss').innerHTML = 'Não';
    document.getElementById('dismiss').style.display = 'block';
    document.getElementById('completeAction').style.display = 'block';
    document.getElementById('completeAction').setAttribute('onclick', action);

}

function showModalBackPage() {
    $('#myModal .modal-body').text("Realmente Deseja Voltar ?");
    $('#myModal').modal('show');
    document.getElementById('completeAction').style.display = 'none';

    document.getElementById('dismiss').innerHTML = 'Não';
    document.getElementById('dismiss').style.display = 'block';
    document.getElementById('verifyBack').style.display = 'block';
}

function verifySave(message, action) {
    var element = document.getElementById('customActived');
    if (element.checked) {
        var options = document.getElementsByName('arrayVotingOptions');
        if (!(options.length > 0)) {
            showModalMessage('É preciso conter no mínimo uma opção de voto !');
        } else {
            showModalCompleteAction('Realmente Deseja Salvar as Opções de Voto Customizadas ' + message + '?', action);
        }
    } else {
        showModalCompleteAction('Realmente Deseja Salvar Opção de Voto Padrão ' + message + '?', action);
    }
}

function saveOptions(iniciar) {
    var element = document.getElementById('customActived');
    var idItem = document.getElementById('itemPauta').getAttribute('value');
    var request = new XMLHttpRequest();
    var data = new FormData();
    var tipoVotacao = 'PADRAO';
    var arrayOptions = [];

    if (element.checked) {
        var options = document.getElementsByName('arrayVotingOptions');
        tipoVotacao = 'CUSTOMIZADA';

        if (options.length > 0) {
            for (i = 0; i < options.length; i++) {
                arrayOptions.push(options[i].innerHTML);
            }
        }
    }

    data.append('arrayOptions', arrayOptions);
    data.append('idItem', idItem);
    data.append('tipoVotacao', tipoVotacao)
    request.open('POST', 'saveOptions', true);
    request.send(data);

    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            if (iniciar) {
                iniciarVotacao();
            } else {
                showModalMessage('Salvo com Sucesso !');
            }
        }
    };

}

function iniciarVotacao() {
    var idItem = document.getElementById('itemPauta').getAttribute('value');
    var reuniaoId = document.getElementById('reuniaoId').getAttribute('value');
    var request = new XMLHttpRequest();
    var data = new FormData();

    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            document.open();
            document.write(this.responseText);
            document.close();
        } else if (this.status === 404) {
            showModalMessage('O Item de pauta: ' + this.responseText + ' já está em votação, cancele ou finalize antes de abrir outro!');
        }
    };

    data.append('idItem', idItem);
    data.append('reuniaoId', reuniaoId);
    request.open('POST', 'abrirVotacao', true);
    request.send(data);
}

