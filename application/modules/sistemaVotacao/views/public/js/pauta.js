$(document).ready(function () {
    $('#customActived').change(function () {
        var request = new XMLHttpRequest();
        var data = new FormData();

        idReuniao = document.getElementsByName('idReuniao')[0].id;

        data.append('idReuniao', idReuniao);
        if ($('#customActived').prop('checked')) {
            data.append('status', 1);
        } else {
            data.append('status', 0);
        }
        request.open('POST', 'atualizarStatusReuniao', true);
        request.send(data);
    });
});