$(document).ready(function () {
    var source = new EventSource("verificarReunioesAbertas"); 
      
    source.addEventListener('message', function(e) {
      if(e.data.includes('id')){
        reunioesAbertas = e.data.split(',');
        ativarReunioesAbertas(reunioesAbertas);
      } else {
        desativasTodasReunioes();
      }
    }, false);

    function ativarReunioesAbertas(reunioesAbertas){
      desativasTodasReunioes();
      for (i = 0; i < reunioesAbertas.length; i++) { 
        var idReuniao = reunioesAbertas[i].replace(/[^0-9]/g,'');
        if(document.getElementById(idReuniao) != null){
          if(document.getElementById(idReuniao).disabled != false){
            document.getElementById(idReuniao).disabled = false;
          }
        }
      }
    }

    function desativasTodasReunioes(){
      botoes = document.getElementsByName('entrarReuniao');
      for (i = 0; i < botoes.length; i++) { 
        botoes[i].disabled = true;
      }
    }

  });