function verificarPresenca() {
    var itemId = document.getElementById('itemPautaId');

    var request = new XMLHttpRequest();
    var data = new FormData();
    request.open('POST', 'verificarFimVotacao', true);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var listaPresenca = JSON.parse(this.responseText);
            atualizarTabela(listaPresenca);
        }else if(this.readyState == 4 && this.status == 202){
            document.open();
            document.write(this.responseText);
            document.close();
            window.location.reload(true);
        }else if(this.readyState === 4 && this.status === 404){
            showModalCancelarAction();
        }
    };

    data.append('itemId', itemId.value);
    request.send(data);
    setTimeout(verificarPresenca, 1000);

};

function atualizarTabela(listaPresenca) {

    var table = document.getElementById('logadosTable');

    for (var i = table.getElementsByTagName('tr').length - 1; i > 0; i--) {
        table.deleteRow(i);
    }

    for (var i in listaPresenca) {
        if (listaPresenca[i]['statusVoto'] == "0") {
            var row = table.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = listaPresenca[i]['idUsuario'];
            cell2.innerHTML = listaPresenca[i]['nome'];
        }
    }

}