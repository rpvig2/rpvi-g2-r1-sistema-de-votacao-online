function votar(opcaoDeVotoId){

    var itemPautaId = document.getElementById('itemPautaId');


    var request = new XMLHttpRequest();
    var data = new FormData();
    request.open('POST', 'salvarVoto', true);

    data.append('itemPautaId', itemPautaId.value);
    data.append('opcaoDeVotoId', opcaoDeVotoId);
    request.send(data);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            window.location.href = this.responseText;
        }
    }

}



function sairVotacao(){

    var itemPautaId = document.getElementById('itemPautaId');

    var request = new XMLHttpRequest();
    var data = new FormData();
    request.open('POST', 'sairVotacao', false);

    data.append('itemPautaId', itemPautaId.value);
    request.send(data);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
        }
    }

}

window.onbeforeunload = function() {
    sairVotacao();
}

function showModalCompleteAction(message, action) {

    $('#myModal .modal-body').text(message);
    $('#myModal').modal('show');    

    document.getElementById('dismiss').innerHTML = 'Não';
    document.getElementById('dismiss').style.display = 'block';
    document.getElementById('completeAction').style.display = 'block';
    document.getElementById('completeAction').setAttribute('onclick', action);

}