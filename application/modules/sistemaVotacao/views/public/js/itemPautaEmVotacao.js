function showModalCompleteAction(message, action){
    $('#confirmModal .modal-body').text(message); 
    $('#confirmModal').modal('show');   

    document.getElementById('dismiss').innerHTML = 'Não';
    document.getElementById('dismiss').style.display = 'block';
    document.getElementById('completeAction').style.display = 'block';
    document.getElementById('completeAction').setAttribute('onclick', action);				
}

function showModalCancelarAction(){
    $('#confirmModal .modal-body').text("Ocorreu um empate a votação será cancelada!"); 
    $('#confirmModal').modal('show');    

    document.getElementById('dismiss').innerHTML = 'Não';
    document.getElementById('dismiss').style.display = 'none';
    document.getElementById('completeAction').style.display = 'block';
    document.getElementById('completeAction').setAttribute('value', "OK");
    document.getElementById('completeAction').setAttribute('onclick', "cancelarVotacao()");				
}

function showModalMessage(message){
    $('#confirmModal .modal-body').text(message); 
    $('#confirmModal').modal('show');    

    document.getElementById('dismiss').innerHTML = 'OK';
    document.getElementById('dismiss').style.display = 'block';
    document.getElementById('completeAction').style.display = 'none';    			
}

function cancelarVotacao(){				
    var idItem = document.getElementById('itemPauta').getAttribute('value');
    var request = new XMLHttpRequest();
    var data = new FormData();
    
    data.append('idItem', idItem);
    
    request.open('POST', 'cancelarVotacao', true);
    request.send(data);

    request.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
            document.open();
            document.write(this.responseText);
            document.close();
        }
    };

}

function finalizarVotacao(){				
    var idItem = document.getElementById('itemPauta').getAttribute('value');
    var request = new XMLHttpRequest();
    var data = new FormData();
    
    data.append('itemId', idItem);
    data.append('forcarFim', 1);

    request.open('POST', 'verificarFimVotacao', true);
    request.send(data);

    request.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 202){
            document.open();
            document.write(this.responseText);
            document.close();
        }else if(this.readyState === 4 && this.status === 404){
            showModalCancelarAction();
        }else if(this.readyState === 4 && this.status === 200){
            showModalMessage("Esta votação não possui corum suficiente para ser finalizada");
        }
    };
}

function voltar(){				
    var reuniaoId = document.getElementById('reuniaoId').getAttribute('value');
    var request = new XMLHttpRequest();
    var data = new FormData();
    
    data.append('reuniaoId', reuniaoId);    
    request.open('POST', 'voltarPauta', true);
    request.send(data);

    request.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
            window.location.href = this.responseText;
        }
    };

}	