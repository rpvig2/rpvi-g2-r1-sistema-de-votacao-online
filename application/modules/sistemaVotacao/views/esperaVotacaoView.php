<?php
    defined('BASEPATH') or exit('No direct script access allowed');
    $data['title'] = "Votação";    
    $this->load->view('templates/header', $data);
?>
<script src="<?php echo base_url('/application/modules/sistemaVotacao/views/public/js/esperaVotacao.js')?>"></script>
</head>
<body>
<div class="container"> 	
    
    <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
        <div class="lh-100">
            <h1 class="mb-0 text-white lh-100">Aguardando item de pauta para votação!</h1>
            <input type="hidden" id="reuniaoId" name="reuniaoId" value="<?php echo $reuniaoId; ?>"/>
        </div>
    </div>
</div>

<script>
    !verificarVotacao();
</script>

<?php $this->load->view('templates/footer');?>