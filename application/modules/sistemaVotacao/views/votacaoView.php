<?php
    defined('BASEPATH') or exit('No direct script access allowed');
    $data['title'] = "Votação";
    $this->load->view('templates/header', $data);
?>
<script src="<?php echo base_url('/application/modules/sistemaVotacao/views/public/js/votacao.js')?>"></script>
</head>
<body>
<div class="container"> 	
    <?php if(isset($itemDePauta)):?>
        <input id="itemPautaId" type="hidden" value="<?php echo $itemDePauta->getId()?>"/>         
        <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow tableHeader">       
            <div class="lh-100">
                <h1 class="mb-0 text-white lh-100"><?php echo $itemDePauta->getDescricao()?></h1>        
            </div>
        </div>     
        <table class="table" id="votacaoTable">
        <thead>
            <tr>        
                <th scope="col" >Opção</th>          
                <th scope="col">Ação</th>
            </tr>
        </thead>  
        <tbody>   
            <?php foreach ($itemDePauta->getOpcaoDeVotoList() as $opcaoDeVoto): ?>   
                <tr>      
                    <td> 
                        <?php echo $opcaoDeVoto->getDescricao()?>                
                    </td>            
                    <td>                        
                        <button class="btn btn-lg" type="submit" 
                        onclick="showModalCompleteAction('Deseja votar na opção: <?php echo $opcaoDeVoto->getDescricao()?>', 'votar(<?php echo $opcaoDeVoto->getId()?>)')">Votar</button>                            
                    </td>     
                </tr>
            <?php endforeach?>
        </tbody>
    </table>
    <?php else: ?>
        <?php echo base_url('votar')?>
    <?php endif ?>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="titleModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="titleModalLabel">Informações</h4>
                </div>
            <div class="modal-body">
                
            </div>
                <div class="modal-footer">   
                    <button id='dismiss' type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>                   
                    <button id='completeAction' type="button" class="btn btn-unipampa">Sim</button>                 
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('templates/footer');?>