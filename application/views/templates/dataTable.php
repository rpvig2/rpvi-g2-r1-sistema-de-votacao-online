<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
  
<?php foreach ($idTable as $id):?>  
<script>
    $(document).ready( function () {
        $('#<?php echo $id?>').DataTable();
    } );
    $.extend( $.fn.dataTable.defaults, {
        searching: false,
        ordering:  false
    } );
</script>
<?php endforeach?>